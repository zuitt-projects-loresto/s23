db.users.insertMany([
	{
		"firstName" : "Akihisa",
		"lastName" : "Yoshii",
		"email" : "aki.yoshii@bakatest.co",
		"password" : "P@55w0rd",
		"isAdmin" : false
	},
	{
		"firstName" : "Mizuki",
		"lastName" : "Himeji",
		"email" : "m.himeji@bakatest.co",
		"password" : "21d3c1995",
		"isAdmin" : false
	},
	{
		"firstName" : "Hideyoshi",
		"lastName" : "Kinoshita",
		"email" : "hideyoshi@bakatest.co",
		"password" : "!!!HIDEYOSHI!!!",
		"isAdmin" : false
	},
	{
		"firstName" : "Yuuji",
		"lastName" : "Sakamoto",
		"email" : "yuusakamoto@bakatest.co",
		"password" : "ashiteruShouko",
		"isAdmin" : false
	},
	{
		"firstName" : "Minami",
		"lastName" : "Shimada",
		"email" : "m.shimada@bakatest.co",
		"password" : "sUmm0n",
		"isAdmin" : false
	}
	])

db.courses.insertMany([
	{
		"name" : "English",
		"price" : 5000,
		"isActive" : false,
	},
	{
		"name" : "Computer",
		"price" : 8000,
		"isActive" : false,
	},
	{
		"name" : "Technology",
		"price" : 8000,
		"isActive" : false,
	}
	])

db.users.find({"isAdmin" : false})

db.users.updateOne({},{$set: {"isAdmin" : true}})

db.courses.updateOne({"name" : "Technology"}, {$set: {"isActive" : true}})

db.courses.deleteMany({"isActive" : false})